<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <title>Product Add</title>
</head>
<body>
<nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand">Add Product</a>
            <form action="../control.php" method="POST">
            <button class="btn btn-outline-danger" type="submit" id="cancel" name="action" value='cancel'>CANCEL</button>
            </form>
        </div>
</nav>
<form method="POST" action="../add-product/" class="frmlag" id="product_form">
  <div class="form-group row">
    <label for="SKU" class="col-sm-2 col-form-label">SKU</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="sku" placeholder="SKU" required="required" name="SKU" value="<?php echo $_POST['SKU'] ?? ''; ?>">
    </div>
  </div>
  <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" placeholder="Name" required="required" name="name" value="<?php echo $_POST['name'] ?? ''; ?>">
    </div>
  </div>
  <div class="form-group row">
    <label for="price" class="col-sm-2 col-form-label">Price</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" id="price" min="0" placeholder="Price" required="required" name="price" value="<?php echo $_POST['price'] ?? ''; ?>">
    </div>
  </div>
  <div>
  <label for="Type" class="form-label">Type Switcher</label>
    <select class="form-select" id="productType" name="sellist" onchange="this.form.submit()">
      <option selected value="0">Choose type...</option>
      <option value="Book">Book</option>
      <option value="DVD">DVD</option>
      <option value="Furniture">Furniture</option>
    </select>
  </div>
  <?php
    //   ob_start();
      require_once('../products/classes.php');

      $pdo = new PDO('mysql:host=localhost;dbname=id18813180_task', 'id18813180_abdelrahman', '@Bdu.2001105');

      if(isset($_POST['sellist'])){
        $type = $_POST['sellist'];
        echo "<br>";
        if($type=="DVD"){
          echo '<div class="form-group row" id="DVD">'.PHP_EOL;
          echo '<label for="size" class="col-sm-2 col-form-label">Size(MB)</label>'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<input type="number" class="form-control" min="0" id="size" name="size" placeholder="Size" required="required">'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;

          echo '<div class="form-group row">'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<button type="submit" class="btn btn-primary" name="addC">Save</button>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
        }elseif($type=="Book"){
          echo '<div class="form-group row" id="Book">'.PHP_EOL;
          echo '<label for="weight" class="col-sm-2 col-form-label">Weight(KG)</label>'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<input type="number" class="form-control" min="0" id="weight" name="weight" placeholder="Weight" required="required">'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;

          echo '<div class="form-group row">'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<button type="submit" class="btn btn-primary" name="addB">Save</button>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
        }elseif($type=="Furniture"){
          echo '<div id="Furniture">'.PHP_EOL;
          
          echo '<div class="form-group row">'.PHP_EOL;
          echo '<label for="height" class="col-sm-2 col-form-label">Height(CM)</label>'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<input type="number" class="form-control" min="0" id="height" name="height" placeholder="Height" required="required">'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          
          echo '<div class="form-group row">'.PHP_EOL;
          echo '<label for="width" class="col-sm-2 col-form-label">Width(CM)</label>'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<input type="number" class="form-control" min="0" id="width" name="width" placeholder="Width" required="required">'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          
          echo '<div class="form-group row">'.PHP_EOL;
          echo '<label for="length" class="col-sm-2 col-form-label">Length(CM)</label>'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<input type="number" class="form-control" min="0" id="length" name="length" placeholder="Length" required="required">'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;

          echo '<div class="form-group row">'.PHP_EOL;
          echo '<div class="col-sm-10">'.PHP_EOL;
          echo '<button type="submit" class="btn btn-primary" name="addF">Save</button>'.PHP_EOL;
          echo '</div>'.PHP_EOL;
          echo '</div>'.PHP_EOL;

          echo '</div>'.PHP_EOL;
        }
      }
      if(isset($_POST['addC'])){
        $CD = new CD($_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['size']);
        $arr = array($CD->getSKU(), serialize($CD));
        $stmt = $pdo->prepare("INSERT into products(SKU, Object) values(?,?)");
        try{
          $stmt->execute($arr);
        //   header('Location: ../');
        echo '<script> location.replace("../"); </script>';
        //   ob_end_flush();
        }catch(Exception $e){
          echo '<p style="color: red; text-align: center;">Invalid SKU</p>';
        }
        
      }

      if(isset($_POST['addB'])){
        $Book = new Book($_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['weight']);
        $arr = array($Book->getSKU(), serialize($Book));
        $stmt = $pdo->prepare("INSERT into products(SKU, Object) values(?,?)");
        try{
          $stmt->execute($arr);
        //   header('Location: ../');
        //   ob_end_flush();
        echo '<script> location.replace("../"); </script>';
        }catch(Exception $e){
          echo '<p style="color: red; text-align: center;">Invalid SKU</p>';
        }
      }

      if(isset($_POST['addF'])){
        $Furniture = new Furniture($_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['height'], $_POST['width'], $_POST['length']);
        $arr = array($Furniture->getSKU(), serialize($Furniture));
        $stmt = $pdo->prepare("INSERT into products(SKU, Object) values(?,?)");
        try{
          $stmt->execute($arr);
        //   header('Location: ../');
        //   ob_end_flush();
        echo '<script> location.replace("../"); </script>';
        }catch(Exception $e){
          echo '<p style="color: red; text-align: center;">Invalid SKU</p>';
        }
      }
      
  ?>
  </form> 
  
</body>
</html>