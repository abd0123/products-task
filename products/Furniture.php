<?php
    //Furniture Class
    class Furniture extends Product{
        private $height;
        private $width;
        private $length;
        
        public function __construct($SKU, $name, $price, $height, $width, $length){
            $this->setSKU($SKU);
            $this->setName($name);
            $this->setPrice($price);
            $this->setHeight($height);
            $this->setWidth($width);
            $this->setLength($length);
        }

        public function setHeight($height){
            $this->height = $height;
        }

        public function setWidth($width){
            $this->width = $width;
        }

        public function setLength($length){
            $this->length = $length;
        }

        public function getSpecial(){
            $h = $this->height;
            $w = $this->width;
            $l = $this->length;
            return "Dimensions: $h x $w x $l";
        }
    }
?>