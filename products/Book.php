<?php
    //Book Class
    class Book extends Product{
        private $weight;
        
        public function __construct($SKU, $name, $price, $weight){
            $this->setSKU($SKU);
            $this->setName($name);
            $this->setPrice($price);
            $this->setWeight($weight);
        }

        public function setWeight($weight){
            $this->weight = $weight;
        }

        public function getSpecial(){
            $w = $this->weight;
            return "Weight: $w KG";
        }
    }
?>