<?php
    //Parent Class of all products
    abstract class Product{
        private $SKU;
        private $name;
        private $price;

        public function setSKU($SKU){
            $this->SKU = $SKU;
        }

        public function getSKU(){
            return $this->SKU;
        }

        public function setName($name){
            $this->name = $name;
        }

        public function getName(){
            return $this->name;
        }

        public function setPrice($price){
            $this->price = $price;
        }

        public function getPrice(){
            $p = $this->price;
            return "$p $";
        }

        abstract public function getSpecial();
    }

    


?>