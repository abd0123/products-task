<?php
    //CD Class
    class CD extends Product{
        private $size;
        
        public function __construct($SKU, $name, $price, $size){
            $this->setSKU($SKU);
            $this->setName($name);
            $this->setPrice($price);
            $this->setSize($size);
        }

        public function setSize($size){
            $this->size = $size;
        }

        public function getSpecial(){
            $s = $this->size;
            return "Size: $s MB";
        }
    }

    
    

?>