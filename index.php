<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
     <form class="container-fluid"  action="control.php" method="POST">
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand">Product List</a>
            <button class="btn btn-outline-success" type="submit" id="add-product-btn" name="action" value='0'>ADD</button>
            <button class="btn btn-outline-danger" type="submit" id="delete-product-btn" name="action" value='1'>MASS DELETE</button>
        </div>
    </nav>
    <?php
        require_once('products/classes.php');
        $pdo = new PDO('mysql:host=localhost;dbname=id18813180_task', 'root', '');
        $stmt = $pdo->prepare("SELECT * FROM products");
        $stmt->execute(array());
        $results = $stmt->fetchAll();
        $n = count($results);
        for($i = 0; $i < $n;){
            echo '<div class="row">'.PHP_EOL;
            for($c = 0; $c < 4 and $i < $n; $c++,$i++){
                    $SKU = $results[$i][0];
                    $obj = unserialize($results[$i][1]);
                    $name = $obj->getName();
                    $price = $obj->getPrice();
                    $special = $obj->getSpecial();
                    echo '<div class="col-md-2 txt" >'.PHP_EOL;
                    echo '<input type="checkbox" class="delete-checkbox" id="'.$SKU.'" name="product[]" value="'.$SKU.'"></br>'.PHP_EOL;
                    echo '<label  for="'.$SKU.'">'.$SKU.'</br>'.$name.'</br>'.$price.'</br>'.$special.'</label>'.PHP_EOL;
                    echo '</div>'.PHP_EOL;
            }
            echo "</div>".PHP_EOL;
            echo"</br>".PHP_EOL;
        }

       
        
    
    ?>  
    
    </form>
</body>
</html>